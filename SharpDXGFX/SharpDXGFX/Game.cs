﻿//Based on http://www.johanfalk.eu/blog/tag/sharpdx-beginners-tutorial

using SharpDX.Direct3D;
using SharpDX.DXGI;
using SharpDX.Windows;
using System;
using System.Drawing;
using D3D11 = SharpDX.Direct3D11;
using SharpDXDemo.Data;
using SharpDXDemo.Content;
using Newtonsoft.Json;

namespace SharpDXDemo
{
    public class Game : IDisposable, IGameShared
    {
        private RenderForm _renderForm;

        private Settings _settings;

        private int Width
        {
            get
            {
                return _settings.Width;
            }
        }
        private int Height
        {
            get
            {
                return _settings.Height;
            }
        }

        private D3D11.Device _d3dDevice;
        private D3D11.DeviceContext _d3dDeviceContext;
        private SwapChain _swapChain;
        private D3D11.RenderTargetView _renderTargetView;

        private readonly ContentLoader _contentLoader = new ContentLoader();
        public ContentLoader ContentLoader { get { return _contentLoader; } }

        /// <summary>
        /// Create and initialize a new game.
        /// </summary>
        public Game()
        {
            //Load the settings
            _settings = JsonConvert.DeserializeObject<Settings>(_contentLoader.LoadSettings());

            // Set window properties
            _renderForm = new RenderForm(_settings.Name);
            _renderForm.ClientSize = new Size(Width, Height);
            _renderForm.AllowUserResizing = false;

            InitializeDeviceResources();
        }

        /// <summary>
        /// Start the game.
        /// </summary>
        public void Run()
        {
            // Start the render loop
            RenderLoop.Run(_renderForm, RenderCallback);
        }

        private void RenderCallback()
        {
            Draw();
        }

        private void InitializeDeviceResources()
        {
            ModeDescription backBufferDesc = new ModeDescription(Width, Height, new Rational(60, 1), Format.R8G8B8A8_UNorm);

            // Descriptor for the swap chain
            SwapChainDescription swapChainDesc = new SwapChainDescription()
            {
                ModeDescription = backBufferDesc,
                SampleDescription = new SampleDescription(1, 0),
                Usage = Usage.RenderTargetOutput,
                BufferCount = 1,
                OutputHandle = _renderForm.Handle,
                IsWindowed = true
            };

            // Create device and swap chain
            D3D11.Device.CreateWithSwapChain(DriverType.Hardware, D3D11.DeviceCreationFlags.None, swapChainDesc, out _d3dDevice, out _swapChain);
            _d3dDeviceContext = _d3dDevice.ImmediateContext;

            // Create render target view for back buffer
            using (D3D11.Texture2D backBuffer = _swapChain.GetBackBuffer<D3D11.Texture2D>(0))
            {
                _renderTargetView = new D3D11.RenderTargetView(_d3dDevice, backBuffer);
            }

            // Set back buffer as current render target view
            _d3dDeviceContext.OutputMerger.SetRenderTargets(_renderTargetView);
        }

        /// <summary>
        /// Draw the game.
        /// </summary>
        private void Draw()
        {
            // Clear the screen
            var color = new SharpDX.Mathematics.Interop.RawColor4(0, 0, 0, 1);
            _d3dDeviceContext.ClearRenderTargetView(_renderTargetView, color);

            // Swap front and back buffer
            _swapChain.Present(1, PresentFlags.None);
        }

        public void Dispose()
        {
            _renderTargetView.Dispose();
            _swapChain.Dispose();
            _d3dDevice.Dispose();
            _d3dDeviceContext.Dispose();
            _renderForm.Dispose();
        }
    }
}