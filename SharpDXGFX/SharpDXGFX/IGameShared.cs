﻿using SharpDXDemo.Content;

namespace SharpDXDemo
{
    interface IGameShared
    {
        ContentLoader ContentLoader { get; }
    }
}
