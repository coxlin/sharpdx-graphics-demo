﻿namespace SharpDXDemo.Data
{
    public struct Settings
    {
        public int Width;
        public int Height;
        public string Name;
        public int RefreshRate;
    }
}
