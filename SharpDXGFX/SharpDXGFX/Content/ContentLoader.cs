﻿using System.IO;
using System.Reflection;

namespace SharpDXDemo.Content
{
    public sealed class ContentLoader
    {
        private readonly string _contentLocation;

        public ContentLoader()
        {
            _contentLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); ;
            _contentLocation = Path.Combine(_contentLocation, "Content");
        }
        
        public string LoadSettings()
        {
            string jsonFile = Path.Combine(_contentLocation, "Settings.json");
            return File.ReadAllText(jsonFile);
        } 
    }
}
